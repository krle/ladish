#!/bin/bash

set -e

CONFIG=$(readlink -f $(dirname $0)/..)
PROGRAM=".ladish"
cd ~
rm -rf $PROGRAM
ln -s $CONFIG $PROGRAM
